import numpy as np
import pandas as pd
from typing import List, Dict
from functools import reduce

class Normalizer:
    def __init__(self,
                 tables : List[Dict],
                 fds : List[Dict[str, List[str]]],
                 mvds : List[Dict[str, List[str]]] = None) -> None:

        # List of dictionaries containing the 'Inputparser.relations' variable
        self.tables = tables

        # List of functional dependencies
        self.fds = fds

        # List of multi-valued dependencies
        self.mvds = mvds

        # List of dictionaries created from the original table
        self.normalized_tables = []

    def normalize_to_1nf(self,
                         relations : List[Dict [List[str], List[List[str]]] ],
                         multi_valued_attributes : List[str] = None) -> None:
        """
        Normalize to 1NF by decomposing multi-valued attributes
        """
        if not multi_valued_attributes:
            # If no multi-valued attributes are specified, return the original relations
            return relations

        normalized_relations = []

        for relation in relations:
            rel_attributes : List[str] = relation["attributes"]
            primary_key : List[str] = relation.get("primary_key", [])

            # Check if the relation contains any multi-valued attributes to be normalized
            mva_list : List[str] = [attr for attr in multi_valued_attributes if attr in rel_attributes]
            
            if mva_list:
                # Normalize each multi-valued attribute separately
                for mva in mva_list:

                    normalized_relations.append({
                        "name": f"{relation['name']}_1NF_{mva}",
                        "attributes": [attr for attr in rel_attributes if attr != mva] + [mva],
                        "primary_key": primary_key
                    })

                # Create a base relation excluding the multi-valued attributes
                new_attributes = [attr for attr in rel_attributes if attr not in mva_list]

                normalized_relations.append({
                    "name": f"{relation['name']}_1NF_base",
                    "attributes": new_attributes,
                    "primary_key": primary_key
                })

            else:
                # If no multi-valued attributes, add the relation as is
                normalized_relations.append(relation)

        self.normalized_tables = normalized_relations

    def normalize_to_2nf(self):
        """
        Normalize to 2NF by decomposing partial dependencies
        """
        new_tables = []

        def is_partial_dependency(fd, primary_key):
            lhs, rhs = fd["lhs"], fd["rhs"]
            lhs_set = set(lhs)
            primary_key_set = set(primary_key)
            # Check if LHS is a proper subset of the primary key
            return lhs_set.issubset(primary_key_set) and lhs_set != primary_key_set

        for relation in self.tables:
            relation_name = relation["name"]
            attributes = relation["attributes"]
            primary_key = relation.get("primary_key", [])
            
            # Identify partial dependencies
            partial_dependencies = [fd for fd in self.fds if is_partial_dependency(fd, primary_key)]

            if not partial_dependencies:
                # If no partial dependencies, add the relation as is
                new_tables.append(relation)
                continue
            
            decomposed_relations = []
            used_attrs = set()

            # Decompose based on partial dependencies
            for pd in partial_dependencies:

                lhs_set = set(pd["lhs"])
                rhs_set = set(pd["rhs"])
                
                dependent_attributes = lhs_set.union(rhs_set)
                
                new_relation_attrs = list(dependent_attributes)
                used_attrs.update(dependent_attributes)

                # Define new relation for partial dependency
                new_relation = {
                    "name": f"{relation_name}_2NF_" + '_'.join(pd["lhs"]),
                    "attributes": new_relation_attrs,
                    #"rows": [{attr: row[attr] for attr in new_relation_attrs} for row in relation['rows']],
                    "primary_key": list(lhs_set)  # lhs_set serves as the primary key for the new relation
                }
                decomposed_relations.append(new_relation)
            
            # Define the base relation excluding all decomposed attributes
            remaining_attrs =  list(set(relation['attributes']) - used_attrs) + primary_key
            if remaining_attrs:
                remaining_relation = {
                    "name": f"{relation_name}_2NF_base",
                    "attributes": remaining_attrs,
                    #"rows": [{attr: row[attr] for attr in remaining_attrs} for row in relation['rows']],
                    "primary_key": primary_key  # Retain the original primary key for the base relation
                }
                decomposed_relations.append(remaining_relation)

            new_tables.extend(decomposed_relations)
            
        unique_relations = self.remove_duplicates(new_tables)
        self.normalized_tables = unique_relations

    def normalize_to_3nf(self):
        '''
        Normalize to 3NF by decomposing transitive dependencies
        '''
        new_tables = []
        for table in self.normalized_tables:
            primary_key = table['primary_key']
            attributes = table["attributes"]
            transitive_dependencies = []
            for fd in self.fds:
                lhs, rhs = fd["lhs"], fd["rhs"]
                if set(lhs).issubset(set(attributes)) and set(rhs).issubset(set(attributes)) and set(lhs).isdisjoint(primary_key) and set(rhs).isdisjoint(primary_key) and set(lhs) != set(primary_key):
                    transitive_dependencies.append(fd)
            if not transitive_dependencies:
                new_tables.append(table)
                continue
            for td in transitive_dependencies:
                new_attributes = td['lhs'] + td['rhs']
                new_relation = {
                    "name": f"{table['name']}_3NF_{'_'.join(td['lhs'])}",
                    "attributes": new_attributes,
                    #"rows": [{attr: row[attr] for attr in new_attributes} for row in table['rows']],
                    "primary_key": td['lhs']
                }
                new_tables.append(new_relation)
            remaining_attributes = list(set(table['attributes']) - set(attr for td in transitive_dependencies for attr in td['rhs']))
            remaining_relation = {
                "name": f"{table['name']}_3NF_remain",
                "attributes": remaining_attributes,
                #"rows": [{attr: row[attr] for attr in remaining_attributes} for row in table['rows']],
                "primary_key": table["primary_key"]
            }
            new_tables.append(remaining_relation)
        unique_relations = self.remove_duplicates(new_tables)
        self.normalized_tables = unique_relations

    def normalize_to_bcnf(self):
        '''
        Normalize to BCNF by decomposing non primary key lhs functional dependencies
        '''
        new_tables = []
        for table in self.normalized_tables:
            primary_key = table['primary_key']
            attributes = table['attributes']
            bcnf_violations = [fd for fd in self.fds if not set(fd['lhs']).issubset(set(primary_key)) and set(fd['lhs']).issubset(set(attributes))] #and set(fd['rhs']).issubset(set(attributes))]
            if bcnf_violations:
                rhs = []
                for fd in bcnf_violations:
                    new_table = {
                        'name': f"{table['name']}_{'_'.join(fd['lhs'])}_bcnf",
                        'attributes': fd['lhs'] + fd['rhs'],
                        'primary_key': fd['lhs']
                    }
                    rhs.extend(fd['rhs'])
                    new_tables.append(new_table)

                remaining_attrs = set(attributes) - set(rhs)

                new_table = {
                    'name': f"{table['name']}_remaining",
                    'attributes': list(remaining_attrs),
                    'primary_key': primary_key
                }
                new_tables.append(new_table)
            else:
                new_tables.append(table)
        unique_relations = self.remove_duplicates(new_tables)
        self.normalized_tables = unique_relations

    def normalize_to_4nf(self):
        '''
        Normalize to 4NF by decomposing table with mvds
        '''
        if self.check_4nf(self.normalized_tables, self.mvds):
            # The table is already in 4NF
            return
        
        new_tables = []
        to_be_removed_mvds = self.mvds.copy()
        for table in self.normalized_tables:
            primary_key = table['primary_key']
            mvd_violations = [mvd for mvd in to_be_removed_mvds if not set(mvd['lhs']).issubset(set(primary_key)) and set(mvd['rhs']).issubset(set(primary_key))]

            if mvd_violations:
                for mvd in mvd_violations:
                    new_table = {
                        'name': f"{table['name']}_{'_'.join(mvd['lhs'])}_4nf",
                        'attributes': mvd['lhs'] + mvd['rhs'],
                        'primary_key': mvd['lhs']
                    }
                    new_tables.append(new_table)
                    #to_be_removed_mvds.remove(mvd)

                remaining_attrs = set(table['attributes']) - {attr for mvd in mvd_violations for attr in mvd['rhs']}
                new_table = {
                    'name': f"{table['name']}_remaining",
                    'attributes': list(remaining_attrs),
                    'primary_key': primary_key
                }
                new_tables.append(new_table)
            else:
                new_tables.append(table)

        self.normalized_tables = new_tables

    def normalize_to_5nf(self):
        '''
        Normalize to 5NF by decomposing tables with more than 1 primary key and join them back and check for lossless join
        '''
        new_tables = []
        decomposed_flag = False # check if we need to decomposed to 5NF
        for table in self.normalized_tables:
            primary_key=table["primary_key"]
            attributes = table["attributes"]
            #print(attributes)
            for fd in self.fds:
                lhs = fd["lhs"]
                rhs = fd["rhs"]
                # If the table has more 2 primary keys, its fd lhs and rhs are the subset of that table attributes and its fd lhs and rhs are not the only attributes in the table
                if len(primary_key)>1 and set(lhs+rhs).issubset(set(attributes)) and set(lhs+rhs)!=(set(attributes)) and set(lhs) != set(primary_key): 
                    # Decompose the table where there is only 1 primary key for each table
                    non_candidate_attributes = set(attributes) - set(primary_key)
                    decomposed_tables = []
                    for key in primary_key:
                        new_attributes = list(non_candidate_attributes)
                        new_attributes.append(key)
                        new_table = {
                            'name': f"{table['name']}_decomposed_with_{key}",
                            'attributes': new_attributes,
                            'primary_key': [key]
                        }
                        #print("newtable", new_table)
                        decomposed_tables.append(new_table)

                    new_decomposed_table_with_data= self.fill_attributes(decomposed_tables, self.tables)
                    #pd.set_option('display.max_colwidth', 100)  # Set column width to 100 characters
                    #print(new_decomposed_table_with_data)
                    joined_table = self.merge_dict_of_lists(new_decomposed_table_with_data)
                    df1 = pd.DataFrame(joined_table)
                    # Join the decomposed tables to 1 single tables
                    tables = []
                    tables.append(table) #turn table into list of dictionary because fill_attributes does not work with only 1 dictionary, need to be a list of dictionary
                    original_table = self.fill_attributes(tables, self.tables)
                    df2 = pd.DataFrame(original_table[table["name"]])
                    # Ensure columns are in the same order
                    df1 = df1[table['attributes']]
                    df2 = df2[table['attributes']]
                    # Sort DataFrames by the same columns
                    df1 = df1.sort_values(by=table['attributes']).reset_index(drop=True)
                    df2 = df2.sort_values(by=table['attributes']).reset_index(drop=True)
                    # Compare DataFrames
                    comparison = df1.equals(df2)
                    # If the joined table is the same as the original table, it is in 5 NF 
                    if not comparison:
                        new_tables.extend(decomposed_tables)
                        decomposed_flag = True # It is already decomposed and added to the final tables, True so it doesn't get add again
                    
                
            if(not decomposed_flag): # If it is not decomposed and already in 5NF, decomposed_flag = False, then add the table to final tables
                new_tables.append(table)
            
            decomposed_flag = False #reset flag

        unique_relations = self.remove_duplicates(new_tables)
        self.normalized_tables = unique_relations
    
    def remove_duplicates(self, relations):
        '''
        Remove duplicated table that is presented in a list of tables
        '''
        unique_relations = {}
        for relation in relations:
            key = (tuple(sorted(relation['attributes'])), tuple(sorted(relation['primary_key'])))
            if key not in unique_relations:
                unique_relations[key] = relation
        return list(unique_relations.values())
    
    def check_4nf(self, tables, mvds):
        '''
        Check if the table is violating 4NF rules or break functional dependencies
        '''
        results = []

        for table in tables:
            table_name = table['name']
            attributes = set(table['attributes'])
            candidate_keys = table['primary_key']

            # 4NF Check
            nontrivial_mvds = self.find_nontrivial_mvds(tables, mvds)
            for mvd in nontrivial_mvds:
                lhs = set(mvd['lhs'])
                rhs = set(mvd['rhs'])

                if lhs.issubset(attributes) and not self.is_superkey(lhs, [candidate_keys]) and rhs.issubset(attributes):
                    results.append({
                        'table': table_name,
                        'type': '4NF Violation',
                        'details': mvd
                    })
        unique_relations = {}
        for relation in results:
            key = (tuple(sorted(relation['type'])), tuple(sorted(relation['details'])))
            if key not in unique_relations:
                unique_relations[key] = relation
        return list(unique_relations.values())
    
    def is_superkey(self, attributes, candidate_keys):
        '''
        Check if the candidate key in a list of attributes
        '''
        for key in candidate_keys:
            if set(key).issubset(attributes):
                return True
        return False

    def find_nontrivial_mvds(self,tables, mvds):
        '''
        Find nontrivial mvds for checking 4NF violationg
        '''
        nontrivial_mvds = []

        # Step 1: Flatten the list of attributes in the tables
        all_attributes = set()
        table_dict = {}
        for table in tables:
            table_dict[table['name']] = table['attributes']
            all_attributes.update(table['attributes'])

        # Step 2: Identify nontrivial MVDs
        for mvd in mvds:
            lhs = set(mvd['lhs'])
            rhs = set(mvd['rhs'])

            # Find attributes not in LHS or RHS (complement in the schema)
            complement = all_attributes - lhs - rhs

            if complement:
                nontrivial_mvds.append(mvd)

        return nontrivial_mvds
    
    def print_normalized_tables(self):
        '''
        Print tables
        '''
        for rel in self.normalized_tables:
            print(rel)

    def fill_attributes(self,table, table_data):
        '''
        Fill attributes from input table to smaller tables if smaller tables only have attributes and no data
        '''
        # Initialize result dictionary
        result = {entry['name']: [] for entry in table}
        
        # Extract the input table data
        input_table = table_data[0]
        input_attributes = input_table['attributes']
        input_rows = input_table['rows']
        
        # Convert the input rows to a list of dictionaries for easier processing
        input_data = [dict(zip(input_attributes, row)) for row in input_rows]
        # Process each entry in temp4
        for entry in table:
            name = entry['name']
            attributes = entry['attributes']
            for row in input_data:
                filled_row = {attr: row.get(attr, None) for attr in attributes}
                result[name].append(filled_row)

            # Remove duplicates
            unique_result = []
            for item in result[name]:
                if item not in unique_result:
                    unique_result.append(item)
            result[name] = unique_result
        
        return result
    
    def merge_dict_of_lists(self,dict_of_lists):
        '''
        Merge a list of dictionary into usable DataFrame
        '''
        # Convert each list of dictionaries into a DataFrame
        dataframes = {key: pd.DataFrame(value) for key, value in dict_of_lists.items()}
        
        # Identify common keys for merging
        common_keys = set.intersection(*[set(df.columns) for df in dataframes.values()])
        
        # If no common keys are found, raise an exception
        if not common_keys:
            raise ValueError("No common keys found for merging.")
        
        # Convert common_keys set to a list
        common_keys = list(common_keys)
        
        # Use reduce to perform successive merges on the DataFrames
        result = reduce(lambda left, right: pd.merge(left, right, on=common_keys, how='outer'), dataframes.values())
        
        return result
    

    def check_1nf(self, table, multivalued_attributes):
        '''
        Check if the table is violating 1NF rules (Multivalued attributes )
        '''
        if len(multivalued_attributes) > 0:
            return False
        return True

    def check_2nf(self, table):
        '''
        Check if the table is violating 2NF rules (Partial Dependency)
        '''
        primary_key = table['primary_key']
        primary_key_set = set(primary_key)
        for fd in self.fds:
            lhs_set = set(fd['lhs'])
            if lhs_set.issubset(primary_key_set) and lhs_set != primary_key_set:
                return False
        return True

    def check_3nf(self, table):
        '''
        Check if the table is violating 3NF rules (Transitive Dependencies)
        '''
        primary_key = table['primary_key']
        for fd in self.fds:
            lhs, rhs = fd
            if set(lhs).issubset(set(table['attributes'])) and set(rhs).issubset(set(table['attributes'])) and set(lhs).isdisjoint(primary_key) and set(rhs).isdisjoint(primary_key) and set(lhs) != set(primary_key):
                return False
        return True

    def check_bcnf(self, table):
        '''
        Check if the table is violating BCNF rules
        '''
        primary_key = table['primary_key']
        for fd in self.fds:
            if not set(fd['lhs']).issubset(set(primary_key)) and set(fd['lhs']).issubset(set(table['attributes'])):
                return False
        return True

    def determine_highest_normal_form(self, multivalued_attributes):
        '''
        This function will determine the highest normal form for the input table
        '''
        result = "1NF"
        if self.check_1nf(self.tables[0], multivalued_attributes):
            result = "1NF"
        else:
            return result
        
        if self.check_2nf(self.tables[0]):
            result = "2NF"
        else:
            return result
        if self.check_3nf(self.tables[0]):
            result = "3NF"
        else:
            return result
        
        if self.check_bcnf(self.tables[0]):
            result = "BCNF"
        else:
            return result
        
        if self.check_4nf(self.tables, self.mvds):
            result = "4NF"
        else:
            return result
        
    