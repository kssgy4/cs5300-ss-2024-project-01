import csv
from normalizer import Normalizer
from SQLQueryGenerator import SQLQueryGenerator
from typing import List, Dict, Union

class InputParser:
    def __init__(self) -> None:
        # Is a dictionary containing the primary key, the column names, and the table entries
        self.relations : Dict[str, Union[List[str], List[List[str]]] ] = {}

        # Is a list that contains columns that form the primary key
        self.primary_key : List[str] = []

        # List of column names that are multi-valued attributes
        self.multi_valued_attributes : List[str] = []

        # UNUSED
        self.nested_relations = []

        # List of dictionary that contains the functional dependencies
        self.functional_dependencies : List[Dict[str, List[str]]] = []

        # List of dictionary that contains the multivalued dependencies
        self.multi_valued_dependencies : List[Dict[str, List[str]]] = []

    def parse_relations(self) -> None:
        """
        Reads the CSV file from console and sets up the 'self.relations' dictionary with following keys:
        'name' : Name of the csv file without extension
        'attributes' : List of column names
        'rows' : 2D list of table entries    
        """

        file_name = input("Enter the CSV file name (with .csv extension): ").strip()
        
        try:
            with open(file_name, mode='r') as csvfile:
                csvreader = csv.reader(csvfile)
                headers = next(csvreader)  # Read the first row as headers
                rows = [row for row in csvreader]  # Read the remaining rows as data

            if not headers or not rows:
                raise ValueError("The CSV file is empty or improperly formatted.")
            
            # Store the relations in the appropriate structure
            self.relations = {
                "name": file_name.split('.')[0],  # Use the file name (without extension) as the table name
                "attributes": headers,
                "rows": rows
            }
            print(f"Parsed {len(rows)} rows and {len(headers)} columns from '{file_name}'.")

        except FileNotFoundError:
            print(f"File '{file_name}' not found. Please check the file name and try again.")
        except Exception as e:
            print(f"An error occurred while reading the file: {e}")

    def parse_dependencies(self) -> None:
        """
        Parses dependencies into 'self.functional_dependencies' and 'self.multi_valued_dependencies', which are lists
        of dictionaries. The dictionaries have the following key-value pairs:
        'lhs' : Columns of the table which are left hand side of functional/multivalued dependency
        'rhs' : Columns of the table which are right hand side of a given functional/multivalued dependency
        """


        print("Input Functional Dependencies (type “exit” and hit enter to complete your dependency list):")
        while True:
            fd = input(">>> ").strip()
            if fd.lower() == "exit":
                break
            lhs, rhs = fd.split("->")
            lhs = [x.strip() for x in lhs.split(",")]
            rhs = [x.strip() for x in rhs.split(",")]
            self.functional_dependencies.append({"lhs": lhs, "rhs": rhs})

        print("Input Multi-valued Dependencies (type “exit” and hit enter to complete your dependency list):")
        while True:
            mvd = input(">>> ").strip()
            if mvd.lower() == "exit":
                break
            lhs, rhs = mvd.split("->>")
            lhs = [x.strip() for x in lhs.split(",")]
            rhs = [x.strip() for x in rhs.split(",")]
            self.multi_valued_dependencies.append({"lhs": lhs, "rhs": rhs})

    def parse_primary_key(self) -> None:
        """
        Parses the column names which form the primary key. Stored in 'self.primary_key'. 
        """

        print("Enter the primary key (can be composite, comma-separated if multiple):")
        pk = input("Key: ").strip()
        self.primary_key = [x.strip() for x in pk.split(",")]
        
        self.relations.update({'primary_key':self.primary_key})

    def parse_multi_valued_attributes(self) -> None:
        """
        Parses the column names which have multi-valued attributes in 'self.multi_valued_attributes'.
        """

        print("Enter multi-valued attributes (comma-separated, type “exit” and hit enter to complete):")
        while True:
            mva = input(">>> ").strip()
            if mva.lower() == "exit":
                break
            self.multi_valued_attributes.append([x.strip() for x in mva.split(",")])

    def parse_highest_nf(self) -> str:
        """
        Returns highest normal form provided by user.
        """
        return input("Enter the highest normal form to achieve (1NF, 2NF, 3NF, BCNF, 4NF, 5NF): ")

def main():
    parser = InputParser()
    parser.parse_relations()
    parser.parse_dependencies()
    parser.parse_primary_key()  # Get the primary key
    parser.parse_multi_valued_attributes()  # Get multi-valued attributes
    highest_nf = parser.parse_highest_nf()

    # List of relations stored in the table
    relations : List[Dict [str, Union[ List[str], List[List[str]] ] ]] = []

    relations.append(parser.relations) # Initializing the table
    fds = parser.functional_dependencies
    mvds = parser.multi_valued_dependencies
    multi_valued_attributes = parser.multi_valued_attributes

    normalizer = Normalizer(relations, fds, mvds)

    print("Highest normal form of the input table:", normalizer.determine_highest_normal_form(multi_valued_attributes))

    if highest_nf == '1NF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
    elif highest_nf == '2NF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
        normalizer.normalize_to_2nf()
    elif highest_nf == '3NF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
        normalizer.normalize_to_2nf()
        normalizer.normalize_to_3nf()
    elif highest_nf == 'BCNF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
        normalizer.normalize_to_2nf()
        normalizer.normalize_to_3nf()
        normalizer.normalize_to_bcnf()
    elif highest_nf == '4NF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
        normalizer.normalize_to_2nf()
        normalizer.normalize_to_3nf()
        normalizer.normalize_to_bcnf()
        normalizer.normalize_to_4nf()
    elif highest_nf == '5NF':
        normalizer.normalize_to_1nf(relations, multi_valued_attributes)
        normalizer.normalize_to_2nf()
        normalizer.normalize_to_3nf()
        normalizer.normalize_to_bcnf()
        normalizer.normalize_to_4nf()
        normalizer.normalize_to_5nf()
    else:
        print("Invalid normal form specified.")
        return
    #normalizer.print_normalized_tables()
    sql_generator = SQLQueryGenerator(normalizer.normalized_tables)
    create_table_queries = sql_generator.generate_sql()

    print("SQL Queries:")
    for query in create_table_queries:
        print(query)

# Run the main function
if __name__ == "__main__":
    main()

