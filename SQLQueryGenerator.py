class SQLQueryGenerator:
    def __init__(self, tables):
        self.tables = tables

    def generate_sql(self):
        sql_statements = []
        
        for table in self.tables:
            
            sql = self.generate_create_table_sql(table)
            
            sql_statements.append(sql)
        return sql_statements

    def generate_create_table_sql(self, table):
        table_name = table['name']
        attributes = table['attributes']
        primary_key = table['primary_key']
        
        sql = f"CREATE TABLE {table_name} (\n"
        columns = []
        
        for attr in attributes:
            columns.append(f"    {attr} VARCHAR(255) NOT NULL")
        
        columns.append(f"    PRIMARY KEY ({', '.join(primary_key)})")

        for tab in self.tables:
            if tab["name"] == table_name:
                break

            for attr in attributes:
                if attr in tab['primary_key']:
                    columns.append(f"    FOREIGN KEY ({attr}) REFERENCES {tab['name']} ({attr})")
                    attributes.remove(attr)
                    
        
        sql += ",\n".join(columns)
        sql += "\n);"
        
        return sql


